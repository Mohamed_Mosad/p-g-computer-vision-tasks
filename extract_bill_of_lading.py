# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 13:27:06 2018

@author: mohamed_mosad
"""

from scan_receipts import scan_receipts
from preprocess import preprocess
import pandas as pd
import cv2 as cv
#from spell_checker import correction
import pytesseract
config = ('-l eng --oem 1 --psm 3')
pre = True
imPath = ("D:\Work_Projects\P&G_OCR\images\marsek.jpeg")
scan = scan_receipts(imPath)
myimage=cv.imread("2.jpg")
process=preprocess()
processed_img = process.start_process(scan.img)

if pre:
    #text = pytesseract.image_to_string(processed_img, config=config)
    data = pytesseract.image_to_data(processed_img, config=config, output_type=pytesseract.Output.STRING)
    with open ("data.txt","w+",encoding="utf-8") as f:
        f.truncate()
        f.write(data[82:])
    dataframe = pd.read_csv('data.txt', sep="	",header=None,error_bad_lines=False,engine=None)
    dataframe.columns = ["level","page_num","block_num","par_num","line_num","word_num","left","top","width","height","conf","text"]
    dataframe=dataframe.dropna()
    word_list=list(dataframe['text'])
    leftlist=list(dataframe['left'])
    toplist=list(dataframe['top'])
    widthlist=list(dataframe['width'])
    heightlist=list(dataframe['height'])
    line_num_list=set(dataframe['line_num'])
    word_num_list=set(dataframe['word_num'])
    print(word_list.index('صنع'))
    num=word_list.index('صنع')
    leftfirst=leftlist[num+4]
    topfirst=toplist[num+4]
    widthfirst=widthlist[num+4]
    heightfirst=heightlist[num+4]
    leftlast=leftlist[num]
    toplast=toplist[num]
    widthlast=widthlist[num]
    heightlast=heightlist[num]

    myimage=cv.rectangle(processed_img,(leftfirst,topfirst),(leftlast+widthlast,toplast+heightlast),(0,255,0),2)
    cv.imwrite("result.jpg",myimage)
    print("ok")


else:
   text = pytesseract.image_to_string(scan.img, config=config)


text=text.split("\n")

Print recognized text
for line in text:
    string = ""
    line=line.split(" ")
    for st in line:
        #string +=" "+correction(st)
        string +=" "+st
    print(string)