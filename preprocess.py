#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 14:21:49 2018

@author: zaghlol
"""
import cv2
import numpy as np
class preprocess:
    def start_process(self,image):
        img=image.copy()
        img = cv2.resize(img, None, fx=1.5, fy=1.5, interpolation=cv2.INTER_CUBIC)
        #noise removal
        # Convert to gray
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = self.remove_shadow(img)
        
#        cv2.imshow("fig",img)
#        cv2.waitKey(0)
#        cv2.destroyAllWindows()
        
        # Apply dilation and erosion to remove some noise
        kernel = np.ones((1, 1), np.uint8)
        img = cv2.dilate(img, kernel, iterations=1)
        img = cv2.erode(img, kernel, iterations=1)
        
            # Apply blur to smooth out the edges
        #img = cv2.blur(img,(5,5))
        #img = cv2.bilateralFilter(img,9,75,75)
        
        # Apply threshold to get image with only b&w (binarization)
        img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        #img=cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, 2)
        return img 
    def remove_shadow(self,image):
        img=image.copy()
        #img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        dilated_img = cv2.dilate(img, np.ones((7,7), np.uint8)) 
        bg_img = cv2.medianBlur(dilated_img, 21)
        diff_img = 255 - cv2.absdiff(img, bg_img)
        norm_img = diff_img.copy() # Needed for 3.x compatibility
        cv2.normalize(diff_img, norm_img, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        _, thr_img = cv2.threshold(norm_img, 230, 0, cv2.THRESH_TRUNC)
        cv2.normalize(thr_img, thr_img, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        return thr_img
    def shapen(self,image):
        img=image.copy()
        #img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        kernel_sharpening = np.array([[-1,-1,-1],
                              [-1,9,-1],
                              [-1,-1,-1]])

        # applying different kernels to the input image
        sharpened = cv2.filter2D(img, -1, kernel_sharpening)
        return sharpened
    
            
        